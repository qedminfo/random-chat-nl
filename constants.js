module.exports = {
  LIKE_STICKER_ID: 369239263222822,
  RANDOM_CHAT: 'random-chat',
  SIMSIMI_CHAT: 'simsimi-chat',
  DEFAULT_BLOCK: 'default',
  IN_CHAT: 'in-chat',
  STOP_CHATTING: 'stop-chat',
  FIND_STRANGER_DELAY: 1500
}