var constants = require('../constants')
var fbgraph = require('../helpers/fbgraph');
var messages = require('../templates/vi/messages')
var models = require('../models');
var simsimi = require('../helpers/simsimi');

function handleEvent(event) {
  var senderID = event.sender.id;

  // check sender state
  models.sync().then(function() {
    // create user state
    return models.UserState.findOrCreate({ where: { fbid: senderID } })
  }).spread(function(state, created) {
    // Each event type has different handler
    if (event.message) {
      handleMessage(event, state);
    } else if (event.postback) {
      handlePostback(event, state);
    } else {
      console.log("Webhook received unknown event: ", event);
    }
  });
}

// handles receiving message
function handleMessage(event, state) {
  var senderID = event.sender.id;
  var recipientID = event.recipient.id;
  var timeOfMessage = event.timestamp;
  var message = event.message;

  // console.log("Received message for user %d and page %d at %d with message:", 
    // senderID, recipientID, timeOfMessage);
  // console.log(JSON.stringify(message));

  switch (state.currentBlock) {
    case constants.DEFAULT_BLOCK:
      // show start message if user is in default block
      fbgraph.sendMessageWithAttachment(state.fbid, messages.startMessage);
      break;
    case constants.SIMSIMI_CHAT:
      // chat with simsimi
      chatWithSimsimi(state, message);
      break;
    case constants.IN_CHAT:
      // chat with stranger
      chatWithStranger(state, message);
      break;
  }
  
}

// handles receiving postback
function handlePostback(event, state) {
  var senderID = event.sender.id;
  var recipientID = event.recipient.id;
  var timeOfPostback = event.timestamp;

  // The 'payload' param is a developer-defined field which is set in a postback 
  // button for Structured Messages. 
  var payload = event.postback.payload;

  // console.log("Received postback for user %d and page %d with payload '%s' " + 
    // "at %d", senderID, recipientID, payload, timeOfPostback);

  switch (payload) {
    case constants.RANDOM_CHAT:
      state.update({
        currentBlock: constants.RANDOM_CHAT,
      }).then(function() {
        setTimeout(matchFindingPair, constants.FIND_STRANGER_DELAY);
        fbgraph.sendMessageWithAttachment(state.fbid, messages.findingStranger);
      });
      break;
    case constants.SIMSIMI_CHAT:
      state.update({
        currentBlock: constants.SIMSIMI_CHAT
      }).then(function() {
        fbgraph.sendMessageWithAttachment(state.fbid, messages.connectedToSimsimi);
      });
      break;
    case constants.DEFAULT_BLOCK:
      state.update({
        currentBlock: constants.DEFAULT_BLOCK
      }).then(function() {
        fbgraph.sendMessageWithAttachment(state.fbid, messages.startMessage);
      });
      break;
  }
}

// Handles different blocks

// handles chat with simsimi
function chatWithSimsimi(state, receivedMessage) {
  var messageText = receivedMessage.text;

  if (hasExitSignal(receivedMessage)) {
    closeChat(state);
  } else if (messageText) {
    // request reply from simsimi
    simsimi.requestReply(
      messageText,
      function() {
        // error occured, just send a smile
        fbgraph.sendTextMessage(state.fbid, ":D");
      },
      function(response) {
        fbgraph.sendTextMessage(state.fbid, response);
      }
    );
    
  }
}

// handles chat with stranger
function chatWithStranger(state, receivedMessage) {
  var messageText = receivedMessage.text;
  var messageAttachments = receivedMessage.attachments;
  var messageStickerId = receivedMessage.sticker_id;
  var partnerId = state.partner;

  if (hasExitSignal(receivedMessage)) {
    closeChat(state.fbid);
  } else if (messageText) {
    // send text to partner
    fbgraph.sendTextMessage(partnerId, messageText);
  } else if (messageAttachments) {
    // send attachments to partner
    messageAttachments.forEach(function(attachment) {
      fbgraph.sendMessageWithAttachment(partnerId, cleanAttachment(attachment));
    });
  }
}

// if there exists two people finding for partner,
// matchs them together
function matchFindingPair() {
  // find two people looking for partner
  models.sync().then(function() {
    return models.UserState.findAll({
      where: { currentBlock: constants.RANDOM_CHAT },
      order: ['updatedAt'],
      limit: 2
    });
  }).then(function(userStates) {
    // implement matching logic
    if (userStates.length < 2) {
      return;
    }
    // when at least 2 people want to chat
    userState1 = userStates[0];
    userState2 = userStates[1];
    // update user1
    userState1.update({
      currentBlock: constants.IN_CHAT,
      partner: userState2.fbid
    }).then(function() {
      fbgraph.sendMessageWithAttachment(userState1.fbid, messages.connectedToStranger);
    });
    // update user2
    userState2.update({
      currentBlock: constants.IN_CHAT,
      partner: userState1.fbid
    }).then(function() {
      fbgraph.sendMessageWithAttachment(userState2.fbid, messages.connectedToStranger);
    });
  });
}

// closes chat
function closeChat(state) {
  var userIds = [state.fbid];
  // if the person is in chat, also close its partner chat
  if (state.currentBlock == constants.IN_CHAT) {
    userId = [state.fbid, state.partnerId];
  }
  models.UserState.update({
    currentBlock: constants.DEFAULT_BLOCK
  }, {
    where: {
      fbid: { $in: userIds }
    }
  }).then(function() {
    userIds.forEach(function(userId) {
      fbgraph.sendMessageWithAttachment(userId, messages.endMessage);
    });
  });
}

// helper functions

// returns true if message has exit signal ('pp' or 'bye')
function hasExitSignal(message) {
  var messageText = message.text.toLowerCase();
  // var messageStickerId = message.sticker_id;

  return messages.endChatTriggers.indexOf(messageText) > -1;
}

// removes unecessary fields in attachment
function cleanAttachment(attachment) {
  var contentAttachments = new Set(['image', 'video', 'file', 'audio']);
  if (contentAttachments.has(attachment.type)) {
    attachment.payload = {
      url: attachment.payload.url
    };
  }
  return attachment;
}

// exports
module.exports = {
  handleEvent: handleEvent
};