var router = require('express').Router();
var config = require('../config');
var eventsHandler = require('./handle_events');

// API /webhook/
// for Facebook verification
router.get('/', function(req, res) {
  if (req.query['hub.verify_token'] === config.WEBHOOK_VERIFY_TOKEN) {
    res.send(req.query['hub.challenge']);
  } else {
    res.send('Error, wrong token');
  }
});

router.post('/', function (req, res) {
  var data = req.body;

  // Make sure this is a page subscription
  if (data.object === 'page') {

    // Iterate over each entry - there may be multiple if batched
    data.entry.forEach(function(entry) {
      var pageID = entry.id;
      var timeOfEvent = entry.time;

      // Iterate over each messaging event
      entry.messaging.forEach(function(event) {
        eventsHandler.handleEvent(event);
      });
    });

    // Assume all went well.
    res.sendStatus(200);
  }
});

module.exports = router;