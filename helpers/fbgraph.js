var config = require('../config');
const request = require('request');

function sendTextMessage(recipientId, messageText) {
  var messageData = {
    text: messageText
  };

  callSendAPI(recipientId, messageData);
}

function sendMessageWithButtons(recipientId, messageText, buttons) {
  var messageData = {
    attachment: {
      type: "template",
      payload: {
        template_type: "button",
        text: messageText,
        buttons: buttons
      }
    }
  };

  callSendAPI(recipientId, messageData);
}

function sendMessageWithAttachment(recipientId, attachment) {
  var messageData = {
    attachment: attachment
  };

  console.log(messageData);

  callSendAPI(recipientId, messageData);
}

function callSendAPI(recipientId, messageData) {
  request({
    uri: 'https://graph.facebook.com/v2.9/me/messages',
    qs: { access_token: config.FB_PAGE_ACCESS_TOKEN },
    method: 'POST',
    json: {
      recipient: {
        id: recipientId
      },
      message: messageData
    }
  }, function (error, response, body) {
    if (error || response.statusCode != 200) {
      console.error("Unable to send message.");
      console.error(response);
      console.error(error);
    }
  });  
}

module.exports = {
  sendTextMessage: sendTextMessage,
  sendMessageWithAttachment: sendMessageWithAttachment,
  sendMessageWithButtons: sendMessageWithButtons
};