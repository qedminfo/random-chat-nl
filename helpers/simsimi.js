var request = require('request');

const TROLI_FB_URL = 'https://api.trolyfacebook.com/chat/';

const SIMSIMI_REQ_URL = 'http://www.simsimi.com/getRealtimeReq';

const SIM_TOKEN = 'UwmPMKoqosEETKleXWGOJ6lynN1TQq18wwvrmCy6IRt';

// reply from simsimi

// find reply on simsimi
function replyFromSimsimi(message, onError, onDone) {
  request({
    url: SIMSIMI_REQ_URL,
    qs: {
      uuid: SIM_TOKEN,
      lc: 'vn',
      ft: 1,
      status: 'W',
      reqText: message
    },
    method: 'GET'
  }, (error, response, body) => {
      // don't know how to answer
      if (error || body.indexOf("502 Bad Gateway") > 0 || body.indexOf("respSentence") < 0) {
        onError();
        return;
      }
      
      let text = JSON.parse(body);
      if (text.status == "200"){
        // got answer
        let response = text.respSentence;
        // delay sending like a boss :)
        let delay = 150 + response.length * (2.5 + Math.random()) * 50;
        setTimeout(onDone, delay, response);
      }
    });
}

// reply from troly fb
function replyFromTrolyFb(message, onError, onDone) {
  request({
    url: TROLI_FB_URL,
    qs: {
      noidung: message,
      tenbot: new Buffer("Sim").toString('base64'),
      loctuxau: 1
    },
    method: 'GET'
  }, function(error, response, body) {
      // don't know how to answer
      if (error || body.indexOf("502 Bad Gateway") > 0 || body.indexOf("messages") < 0) {
        onError();
        return;
      }
      
      let text = JSON.parse(body);
      if (text.messages.length > 0){
        // example response: { "messages": [ {"text": "Thôi tao thua"} ] }
        onDone(text.messages[0].text);
      }
    });
}

module.exports = {
  requestReply: replyFromTrolyFb
}