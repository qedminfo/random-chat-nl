var Sequelize = require('sequelize');
var constants = require('../constants');

var sequelize = new Sequelize(process.env.DATABASE_URL, {
  dialect: 'postgres'
});

// User table
var User = sequelize.define('user', {
  fbid: {
    type: Sequelize.BIGINT,
    primaryKey: true
  },
  first_name: Sequelize.STRING,
  last_name: Sequelize.STRING,
  gender: Sequelize.STRING
});

// user_state table
var UserState = sequelize.define('user_state', {
  fbid: {
    type: Sequelize.BIGINT,
    primaryKey: true
  },
  currentBlock: {
    type: Sequelize.STRING,
    defaultValue: constants.DEFAULT_BLOCK
  },
  time: Sequelize.DATE,
  partner: Sequelize.BIGINT
});

// exports
module.exports = {
  sync: function() {
    return sequelize.sync();
  },
  User: User,
  UserState: UserState
}