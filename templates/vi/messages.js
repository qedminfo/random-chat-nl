var texts = require('./texts');
var constants = require('../../constants');

// json message templates
var mainButtons = [
  {
    type: "postback",
    title: texts.chatWithStrangers,
    payload: constants.RANDOM_CHAT
  },
  {
    type: "postback",
    title: texts.chatWithSimsimi,
    payload: constants.SIMSIMI_CHAT
  }
];

var stopFindingButton = {
  type: "postback",
  title: texts.stopFinding,
  payload: constants.DEFAULT_BLOCK
};

var stopChattingButton = {
  type: "postback",
  title: texts.stopChatting,
  payload: constants.STOP_CHATTING
};

// exports
module.exports = {
  mainButtons: mainButtons,

  endChatTriggers: texts.endChatTriggers,

  // start chat message
  startMessage: {
    type: "template",
    payload: {
      template_type: "generic",
      elements: [
        {
          title: texts.welcomeText,
          subtitle: texts.selectToStart,
          buttons: mainButtons
        }
      ]
    }
  },

  // end chat message
  endMessage: {
    type: "template",
    payload: {
      template_type: "generic",
      elements: [
        {
          title: texts.chatEnded,
          subtitle: texts.continueChat,
          buttons: mainButtons
        }
      ]
    }
  },

  // connected to simsimi message
  connectedToSimsimi: {
    type: "template",
    payload: {
      template_type: "generic",
      elements: [
        {
          title: texts.connectedToSimsimi,
          subtitle: texts.thumbUpOrByeToExit,
          // buttons: mainButtons
        }
      ]
    }
  },

  // finding stranger message
  findingStranger: {
    type: "template",
    payload: {
      template_type: "generic",
      elements: [
        {
          title: texts.findingStranger,
          subtitle: texts.pleaseWait,
          buttons: [ stopFindingButton ]
        }
      ]
    }
  },

  // finding stranger message
  connectedToStranger: {
    type: "template",
    payload: {
      template_type: "generic",
      elements: [
        {
          title: texts.connectedToStranger,
          subtitle: texts.thumbUpOrByeToExit,
          // buttons: mainButtons
        }
      ]
    }
  },
};