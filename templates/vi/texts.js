
module.exports = {

  // String messages
  pageName: "<Page name here>",

  welcomeText: "Chào bạn",

  endChatTriggers: ["pp", "bye"],

  selectToStart: "Chọn 1 trong các chức năng bên dưới để bắt đầu.",

  chatEnded: "Cuộc trò chuyện đã kết thúc",

  continueChat: "Bạn có muốn tiếp tục?",

  chatWithStrangers: "Chat với người lạ",

  chatWithSimsimi: "Chat với simsimi",

  thumbUpOrByeToExit: "Gõ 'pp' hoặc 'bye' để thoát trò chuyện",

  stopFinding: "Dừng tìm",

  stopChatting: "Dừng chat",

  connectedToSimsimi: "Kết nối thành công với simsimi",

  findingStranger: "Đang tìm người lạ",

  pleaseWait: "Vui lòng đợi chút :D",

  connectedToStranger: "Kết nối thành công với người lạ",
};